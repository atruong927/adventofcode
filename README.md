# AdventOfCode
My attempt at solving Advent of Code problems using Java! See https://adventofcode.com/

**NOTE:** Whatever year / day that isn't commented out in [AdventOfCode](/src/main/java/adventofcode/AdventOfCode.java), [Year 2020](/src/main/java/adventofcode/year_2020/Year2020.java), [Year 2021](/src/main/java/adventofcode/year_2021/Year2021.java), and [Year 2022](/src/main/java/adventofcode/year_2022/Year2022.java) is based on what I was last working on.

[[_TOC_]]

## Completed
### Year 2022
* [Day 1](/src/main/java/adventofcode/year_2022/Day1.java)
* [Day 2](/src/main/java/adventofcode/year_2022/Day2.java)
* [Day 3](/src/main/java/adventofcode/year_2022/Day3.java)
* [Day 4](/src/main/java/adventofcode/year_2022/Day4.java)
* [Day 5](/src/main/java/adventofcode/year_2022/Day5.java)
* [Day 6](/src/main/java/adventofcode/year_2022/Day6.java)
### Year 2021
* [Day 1](/src/main/java/adventofcode/year_2021/Day1.java)
* [Day 2](/src/main/java/adventofcode/year_2021/Day2.java)
* [Day 3](/src/main/java/adventofcode/year_2021/Day3.java)
* [Day 4](/src/main/java/adventofcode/year_2021/Day4.java)
* [Day 5](/src/main/java/adventofcode/year_2021/Day5.java)
* [Day 6](/src/main/java/adventofcode/year_2021/Day6.java)
* [Day 7](/src/main/java/adventofcode/year_2021/Day7.java)
* [Day 9](/src/main/java/adventofcode/year_2021/Day9.java)
* [Day 10](/src/main/java/adventofcode/year_2021/Day10.java)
* [Day 11](/src/main/java/adventofcode/year_2021/Day11.java)
### Year 2020
* [Day 1](/src/main/java/adventofcode/year_2020/Day1.java)
* [Day 2](/src/main/java/adventofcode/year_2020/Day2.java)
* [Day 3](/src/main/java/adventofcode/year_2020/Day3.java)
* [Day 4](/src/main/java/adventofcode/year_2020/Day4.java)
* [Day 5](/src/main/java/adventofcode/year_2020/Day5.java)

## WIP
### Year 2022
* [Day 7](/src/main/java/adventofcode/year_2022/Day7.java)
### Year 2021
* [Day 8](/src/main/java/adventofcode/year_2021/Day8.java)
* [Day 12](/src/main/java/adventofcode/year_2021/Day12.java)

## Not started yet
### Year 2022
...
### Year 2021
* [Day 13](/src/main/java/adventofcode/year_2021/Day13.java)
* [Day 14](/src/main/java/adventofcode/year_2021/Day14.java)
* [Day 15](/src/main/java/adventofcode/year_2021/Day15.java)
* [Day 16](/src/main/java/adventofcode/year_2021/Day16.java)
* [Day 17](/src/main/java/adventofcode/year_2021/Day17.java)
### Year 2020
* [Day 6](/src/main/java/adventofcode/year_2020/Day6.java)
