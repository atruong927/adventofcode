package adventofcode.year_template;

/**
 * https://adventofcode.com/yyyy
 *
 * @author Amanda Truong
 */
public class YearTemplate {
	public static void answer() {
		System.out.println("\n**yyyy**");
		DayTemplate.answer();
	}
}
