package adventofcode;

import adventofcode.year_2020.Year2020;
import adventofcode.year_2021.Year2021;
import adventofcode.year_2022.Year2022;

/**
 * https://adventofcode.com
 *
 * @author Amanda Truong
 */
public class AdventOfCode {
	public static void main(String[] args) {
		System.out.println("Advent of Code!");
		// Year2020.answer();
		// Year2021.answer();
		Year2022.answer();
	}
}
