package adventofcode.year_2020;

import java.util.LinkedList;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2020/day/6
 *
 * @author Amanda Truong
 */
public class Day6 {
	private static final String INPUT_FILE_PATH = "resources/2020/day6-input.txt";

	public static void answer() {
		System.out.println("\nDay 6 - ******** WIP ********");

		List<String> exampleLines = new LinkedList<>();

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(exampleLines);
		part2(exampleLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");
	}

	private static void part2(List<String> lines) {
		System.out.println("\nPart 2");
	}
}
