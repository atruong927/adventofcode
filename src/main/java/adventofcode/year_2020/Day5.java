package adventofcode.year_2020;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2020/day/5
 *
 * @author Amanda Truong
 */
public class Day5 {
	private static final String INPUT_FILE_PATH = "resources/2020/day5-input.txt";

	public static void answer() {
		System.out.println("\nDay 5");

		List<String> exampleLines = new LinkedList<>();
		exampleLines.add("FBFBBFFRLR");
		exampleLines.add("BFFFBBFRRR");
		exampleLines.add("FFFBBBFRRR");
		exampleLines.add("BBFFBBFRLL");

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(inputLines);
		part2(inputLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");

		int highestSeatId = 0;
		for (String line : lines) {
			String[] partitions = line.split("");

			// find seat
			int[] seat = findSeat(partitions);

			// calculate seat ID
			int seatId = calcSeatId(seat[0], seat[1]);

			if (seatId > highestSeatId) {
				highestSeatId = seatId;
			}
		}

		System.out.println("highestSeatId=" + highestSeatId);
	}

	/**
	 * Finds the seat.
	 *
	 * @param partitions The partitions. First 7 letters are F/B representing
	 *                   Front/Back. Last 3 letters
	 *                   are L/R representing Left/Right.
	 * @return The seat represented by an array. First element is the row, Second
	 *         element is the column.
	 */
	private static int[] findSeat(String[] partitions) {
		// find row
		int firstRow = 0;
		int lastRow = 127;
		for (int i = 0; i < 7; i++) {
			// partition front (lower half)
			if (partitions[i].equals("F")) {
				lastRow = firstRow / 2 + lastRow / 2;
			}
			// partition back (upper half)
			else if (partitions[i].equals("B")) {
				firstRow = firstRow / 2 + (int) Math.round(lastRow / 2d);
			}
		}

		// find column
		int firstCol = 0;
		int lastCol = 7;
		for (int i = 7; i < partitions.length; i++) {
			// partition left
			if (partitions[i].equals("L")) {
				lastCol = firstCol / 2 + lastCol / 2;
			}
			// partition right
			else if (partitions[i].equals("R")) {
				firstCol = firstCol / 2 + (int) (Math.round(lastCol / 2d));
			}
		}

		return new int[] { firstRow, firstCol };
	}

	/**
	 * Calculates the seat ID.
	 *
	 * @param row The seat's row.
	 * @param col The seat's column.
	 * @return The seat ID.
	 */
	private static int calcSeatId(int row, int col) {
		return row * 8 + col;
	}

	private static void part2(List<String> lines) {
		System.out.println("\nPart 2");

		// find all seat IDs in list
		List<Integer> seatIds = new ArrayList<>();
		seatIds = lines.stream().map(line -> {
			String[] partitions = line.split("");

			// find seat
			int[] seat = findSeat(partitions);

			// calculate seat ID
			return calcSeatId(seat[0], seat[1]);
		}).collect(Collectors.toList());
		Collections.sort(seatIds);

		int lowestSeatId = seatIds.get(0);
		int highestSeatId = seatIds.get(seatIds.size() - 1);

		// find all possible seat IDs from lowest ID and highest ID in list
		List<Integer> allPossibleSeatIds = new ArrayList<>();
		for (int i = lowestSeatId; i <= highestSeatId; i++) {
			allPossibleSeatIds.add(i);
		}

		// find only missing boarding pass in list
		for (Integer seatId : seatIds) {
			allPossibleSeatIds.remove(seatId);
		}
		System.out.println("last seat ID=" + allPossibleSeatIds.get(0));
	}
}
