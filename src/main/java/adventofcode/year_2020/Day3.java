package adventofcode.year_2020;

import java.util.LinkedList;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2020/day/3
 *
 * @author Amanda Truong
 */
public class Day3 {
	private static final String INPUT_FILE_PATH = "resources/2020/day3-input.txt";

	public static void answer() {
		System.out.println("\nDay 3");

		List<String> exampleRows = new LinkedList<>();
		exampleRows.add("..##.........##.........##.........##.........##.........##.......");
		exampleRows.add("#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..");
		exampleRows.add(".#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.");
		exampleRows.add("..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#");
		exampleRows.add(".#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.");
		exampleRows.add("..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....");
		exampleRows.add(".#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#");
		exampleRows.add(".#........#.#........#.#........#.#........#.#........#.#........#");
		exampleRows.add("#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...");
		exampleRows.add("#...##....##...##....##...##....##...##....##...##....##...##....#");
		exampleRows.add(".#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#");

		List<String> inputRows = DataUtil.readFileLines(INPUT_FILE_PATH);

		System.out.println("Part 1");
		part1(inputRows, 3, 1);
		System.out.println("Part 2");
		part2(inputRows);
	}

	private static int part1(List<String> map, int right, int down) {
		// count num trees encountered when sloping down
		int count = 0;
		int column = right;
		for (int row = down; row < map.size(); row += down) {
			if (map.get(row).charAt(column) == '#') {
				count++;
			}

			column += right;
			if (column >= map.get(row).length()) {
				column -= map.get(row).length();
			}
		}

		System.out.println("num trees encountered = " + count);
		return count;
	}

	private static void part2(List<String> map) {
		long case1 = part1(map, 1, 1);
		long case2 = part1(map, 3, 1);
		long case3 = part1(map, 5, 1);
		long case4 = part1(map, 7, 1);
		long case5 = part1(map, 1, 2);

		// get product!
		long product = case1 * case2 * case3 * case4 * case5;
		System.out.println("product = " + product);
	}
}
