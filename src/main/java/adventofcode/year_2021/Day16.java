package adventofcode.year_2021;

import java.util.ArrayList;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/16
 *
 * @author Amanda Truong
 */
public class Day16 {
	private static final String INPUT_FILE_PATH = "resources/2021/day16-input.txt";

	public static void answer() {
		System.out.println("\nDay 16");

		List<String> exampleLines = new ArrayList<>();

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(exampleLines);
		part2(exampleLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");
	}
}
