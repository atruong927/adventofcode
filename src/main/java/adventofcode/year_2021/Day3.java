package adventofcode.year_2021;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/3
 *
 * @author Amanda Truong
 */
public class Day3 {
	private static final String INPUT_FILE_PATH = "resources/2021/day3-input.txt";

	public static void answer() {
		System.out.println("\nDay 3");

		List<String> exampleBinaryNums = new LinkedList<>();
		exampleBinaryNums.add("00100");
		exampleBinaryNums.add("11110");
		exampleBinaryNums.add("10110");
		exampleBinaryNums.add("10111");
		exampleBinaryNums.add("10101");
		exampleBinaryNums.add("01111");
		exampleBinaryNums.add("00111");
		exampleBinaryNums.add("11100");
		exampleBinaryNums.add("10000");
		exampleBinaryNums.add("11001");
		exampleBinaryNums.add("00010");
		exampleBinaryNums.add("01010");

		List<String> inputBinaryNums = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(inputBinaryNums);
		part2(inputBinaryNums);
	}

	private static void part1(List<String> binaryNums) {
		System.out.println("Part 1");

		List<String> mostCommonBitList = new LinkedList<>();
		List<String> leastCommonBitList = new LinkedList<>();
		int tempCounter0Bit = 0;
		int tempCounter1Bit = 0;
		for (int i = 0; i < binaryNums.get(0).length(); i++) {
			// count 0 bits and 1 bits
			for (String binaryNum : binaryNums) {
				String bit = binaryNum.substring(i, i + 1);
				if (bit.equals("0")) {
					tempCounter0Bit++;
				} else {
					tempCounter1Bit++;
				}
			}

			// populate most and least common bit list
			mostCommonBitList.add(tempCounter0Bit > tempCounter1Bit ? "0" : "1");
			leastCommonBitList.add(tempCounter0Bit > tempCounter1Bit ? "1" : "0");

			// reset counter
			tempCounter0Bit = 0;
			tempCounter1Bit = 0;
		}

		// generate gamma rate
		// get most common bits and combine into 1 binary num
		String gammaRateBinary = mostCommonBitList.stream().collect(Collectors.joining());
		// convert binary num to decimal
		int gammaRateDec = Integer.parseInt(gammaRateBinary, 2);

		// generate epsilon rate
		// get least most common bits and combine into 1 binary num
		String epsilonRateBinary = leastCommonBitList.stream().collect(Collectors.joining());
		// convert binary num to decimal
		int epsilonRateDec = Integer.parseInt(epsilonRateBinary, 2);

		// calculate power consumption
		// product of gamma rate (decimal) and epsilon rate (decimal)
		int powerConsumption = gammaRateDec * epsilonRateDec;
		System.out.println("powerConsumption=" + powerConsumption);
	}

	private static void part2(List<String> binaryNums) {
		System.out.println("Part 2");

		// find oxygen generator rating
		String oxygenGeneratorRatingBinary = iterativeMostFilter(binaryNums);
		int oxygenGeneratorRatingDec = Integer.parseInt(oxygenGeneratorRatingBinary, 2);

		// find co2 scrubber rating
		String co2ScrubberRatingBinary = iterativeLeastFilter(binaryNums);
		int co2ScrubberRatingDec = Integer.parseInt(co2ScrubberRatingBinary, 2);

		// calculate life support rating
		// product of oxygen generator rating and co2 scrubber rating
		int lifeSupportRating = oxygenGeneratorRatingDec * co2ScrubberRatingDec;
		System.out.println("lifeSupportRating=" + lifeSupportRating);
	}

	/**
	 * Iteratively filters the list of binary numbers with the MOST common bit until
	 * there is a final binary number.
	 *
	 * @param binaryNums The list of binary numbers
	 * @return The final binary number after iterative filtering for the MOST common
	 *         bit.
	 */
	private static String iterativeMostFilter(List<String> binaryNums) {
		List<String> filteredBinaryNums = new LinkedList<>(binaryNums);
		for (int i = 0; filteredBinaryNums.size() != 1; i++) {
			List<String> mostCommonBitList = getMostCommonBitList(filteredBinaryNums);
			filteredBinaryNums = filterByBitListAtIndex(i, mostCommonBitList, filteredBinaryNums);
		}
		return filteredBinaryNums.get(0);
	}

	/**
	 * Gets the list of MOST common bits for each index in the list of binary
	 * numbers.
	 *
	 * @param binaryNums The list of binary numbers.
	 * @return The list of MOST common bits for each index in the list of binary
	 *         numbers.
	 */
	private static List<String> getMostCommonBitList(List<String> binaryNums) {
		List<String> mostCommonBitList = new LinkedList<>();
		int tempCounter0Bit = 0;
		int tempCounter1Bit = 0;
		for (int i = 0; i < binaryNums.get(0).length(); i++) {
			// count 0 bits and 1 bits
			for (String binaryNum : binaryNums) {
				String bit = binaryNum.substring(i, i + 1);
				if (bit.equals("0")) {
					tempCounter0Bit++;
				} else {
					tempCounter1Bit++;
				}
			}

			// populate most and least common bit list
			// if 0 and 1 are equally common, keep values with a 1 in the position being
			// considered
			mostCommonBitList.add(tempCounter0Bit > tempCounter1Bit ? "0" : "1");

			// reset counter
			tempCounter0Bit = 0;
			tempCounter1Bit = 0;
		}
		return mostCommonBitList;
	}

	/**
	 * Iteratively filters the list of binary numbers with the LEAST common bit
	 * until there is a final binary number.
	 *
	 * @param binaryNums The list of binary numbers
	 * @return The final binary number after iterative filtering for the LEAST
	 *         common bit.
	 */
	private static String iterativeLeastFilter(List<String> binaryNums) {
		List<String> filteredBinaryNums = new LinkedList<>(binaryNums);
		for (int i = 0; filteredBinaryNums.size() != 1; i++) {
			List<String> mostCommonBitList = getLeastCommonBitList(filteredBinaryNums);
			filteredBinaryNums = filterByBitListAtIndex(i, mostCommonBitList, filteredBinaryNums);
		}
		return filteredBinaryNums.get(0);
	}

	/**
	 * Gets the list of LEAST common bits for each index in the list of binary
	 * numbers.
	 *
	 * @param binaryNums The list of binary numbers.
	 * @return The list of LEAST common bits for each index in the list of binary
	 *         numbers.
	 */
	private static List<String> getLeastCommonBitList(List<String> binaryNums) {
		List<String> leastCommonBitList = new LinkedList<>();
		int tempCounter0Bit = 0;
		int tempCounter1Bit = 0;
		for (int i = 0; i < binaryNums.get(0).length(); i++) {
			// count 0 bits and 1 bits
			for (String binaryNum : binaryNums) {
				String bit = binaryNum.substring(i, i + 1);
				if (bit.equals("0")) {
					tempCounter0Bit++;
				} else {
					tempCounter1Bit++;
				}
			}

			// populate most and least common bit list
			// if 0 and 1 are equally common, keep values with a 0 in the position being
			// considered
			leastCommonBitList.add(tempCounter0Bit > tempCounter1Bit ? "1" : "0");

			// reset counter
			tempCounter0Bit = 0;
			tempCounter1Bit = 0;
		}
		return leastCommonBitList;
	}

	/**
	 * Gets the desired bit in the bit list, at index. Filters the list of binary
	 * numbers to only binary numbers whos bit at index matches the desired bit.
	 *
	 * @param index      The desired bit index.
	 * @param bitList    The list of bits.
	 * @param binaryNums The list of binary numbers.
	 * @return The filtered list of binary numbers.
	 */
	private static List<String> filterByBitListAtIndex(int index, List<String> bitList, List<String> binaryNums) {
		// only keep numbers with that bit in that position (index)
		String desiredBit = bitList.get(index);
		List<String> filteredBinaryNums = new LinkedList<>();
		for (String binaryNum : binaryNums) {
			if (binaryNum.substring(index, index + 1).equals(desiredBit)) {
				filteredBinaryNums.add(binaryNum);
			}
		}
		return filteredBinaryNums;
	}
}
