package adventofcode.year_2021;

import java.util.Arrays;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/6
 *
 * @author Amanda Truong
 */
public class Day6 {
	private static final String INPUT_FILE_PATH = "resources/2021/day6-input.txt";

	public static void answer() {
		System.out.println("\nDay 6");

		List<String> exampleLines = Arrays.asList("3,4,3,1,2"); // initial state

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		// calcNumFishies(exampleLines, 80);

		System.out.println("Part 1");
		calcNumFishies(inputLines, 80);

		System.out.println("Part 2");
		calcNumFishies(inputLines, 256);
	}

	/**
	 * Calculates the number of fishies after the specific number of days, starting
	 * with
	 * the initial timers.
	 *
	 * @param lines   The list of lines containing a single line representing the
	 *                initial
	 *                list of fishies.
	 * @param numDays The number of days.
	 */
	private static void calcNumFishies(List<String> lines, int numDays) {
		System.out.println("after " + numDays + " days..");
		long[] countdownCounters = generateFishCountdownCounters(lines.get(0));

		for (int dayNum = 1; dayNum <= numDays; dayNum++) {
			System.out.println("dayNum=" + dayNum);

			// update counters
			long tempCounter = countdownCounters[0];
			countdownCounters[0] = countdownCounters[1];
			countdownCounters[1] = countdownCounters[2];
			countdownCounters[2] = countdownCounters[3];
			countdownCounters[3] = countdownCounters[4];
			countdownCounters[4] = countdownCounters[5];
			countdownCounters[5] = countdownCounters[6];
			countdownCounters[6] = countdownCounters[7] + tempCounter;
			countdownCounters[7] = countdownCounters[8];
			countdownCounters[8] = tempCounter;

			System.out.println("curr size=" + countFishies(countdownCounters));
		}

		System.out.println("num fishies=" + countFishies(countdownCounters));
	}

	/**
	 * Generates an array of fish countdowns.
	 *
	 * @param line The initial line.
	 * @return The array with 9 elements (indices 0-8), each element representing
	 *         the number of fish with the
	 *         matching timer value.
	 */
	private static long[] generateFishCountdownCounters(String line) {
		long[] countdownCounters = new long[9];
		Arrays.asList(line.trim().split(",")).forEach(fishy -> {
			int countdown = Integer.parseInt(fishy);
			countdownCounters[countdown]++;
		});
		return countdownCounters;
	}

	/**
	 * Count number of fishies for each countdown.
	 *
	 * @param countdownCounters The number of fishies for each countdown.
	 * @return The total number of fishies throughout each countdown.
	 */
	private static long countFishies(long[] countdownCounters) {
		long numFish = 0;
		for (long counter : countdownCounters) {
			numFish += counter;
		}
		return numFish;
	}
}
