package adventofcode.year_2021;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/9
 *
 * @author Amanda Truong
 */
public class Day9 {
	private static final String INPUT_FILE_PATH = "resources/2021/day9-input.txt";

	public static void answer() {
		System.out.println("\nDay 9");

		List<String> exampleLines = new ArrayList<>();
		exampleLines.add("2199943210");
		exampleLines.add("3987894921");
		exampleLines.add("9856789892");
		exampleLines.add("8767896789");
		exampleLines.add("9899965678");

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(inputLines);
		part2(inputLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");

		List<List<Integer>> heightmaps = parseHeightmaps(lines);

		// find low points
		List<Integer> lowPoints = new ArrayList<>();
		for (int row = 0; row < heightmaps.size(); row++) {
			List<Integer> heightmap = heightmaps.get(row);
			for (int column = 0; column < heightmap.size(); column++) {
				if (isLowPoint(row, column, heightmaps)) {
					lowPoints.add(heightmap.get(column));
				}
			}
		}

		// find risk levels from low points
		List<Integer> riskLevels = lowPoints.stream().map(lowPoint -> lowPoint + 1).collect(Collectors.toList());

		// find sum of risk levels
		int sum = 0;
		for (Integer riskLevel : riskLevels) {
			sum += riskLevel;
		}
		System.out.println("sum=" + sum);
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");

		List<List<Integer>> heightmaps = parseHeightmaps(lines);
		String[][] marked = new String[heightmaps.size()][heightmaps.get(0).size()];

		// find basins
		List<Integer> basinSizes = new ArrayList<>();
		for (int row = 0; row < heightmaps.size(); row++) {
			List<Integer> heightmap = heightmaps.get(row);
			for (int column = 0; column < heightmap.size(); column++) {
				if (isLowPoint(row, column, heightmaps)) {
					// count basin from low point
					int basinSize = getBasinSize(row, column, heightmaps, marked);
					System.out.println("basinSize=" + basinSize);
					basinSizes.add(basinSize);
				}
			}
		}
		System.out.println("basinSizes=" + basinSizes);
		// find 3 largest basins
		Collections.sort(basinSizes);
		basinSizes = basinSizes.subList(basinSizes.size() - 3, basinSizes.size());
		System.out.println("largest basinSizes=" + basinSizes);
		// find the prodcut of the 3 largest basins
		int product = basinSizes.get(0) * basinSizes.get(1) * basinSizes.get(2);
		System.out.println("product=" + product);
	}

	/**
	 * Parse heightmaps.
	 *
	 * @param lines The lines.
	 * @return The list of heightmaps, each represented by a list of Integers.
	 */
	private static List<List<Integer>> parseHeightmaps(List<String> lines) {
		return lines.stream()
				.map(line -> line.split(""))
				.map(valuesArray -> Arrays.asList(valuesArray).stream()
						.map(value -> Integer.parseInt(value))
						.collect(Collectors.toList()))
				.collect(Collectors.toList());
	}

	/**
	 * Whether or not the height at the row and column is a low point.
	 *
	 * @param row        The row.
	 * @param column     The column.
	 * @param heightmaps The height maps.
	 * @return True if the height at the row and column is a low point. False
	 *         otherwise.
	 */
	private static boolean isLowPoint(int row, int column, List<List<Integer>> heightmaps) {
		int currHeight = heightmaps.get(row).get(column);
		int heightUp = row > 0 ? heightmaps.get(row - 1).get(column) : Integer.MAX_VALUE;
		int heightDown = row < heightmaps.size() - 1 ? heightmaps.get(row + 1).get(column) : Integer.MAX_VALUE;
		int heightLeft = column > 0 ? heightmaps.get(row).get(column - 1) : Integer.MAX_VALUE;
		int heightRight = column < heightmaps.get(row).size() - 1 ? heightmaps.get(row).get(column + 1)
				: Integer.MAX_VALUE;
		return currHeight < heightUp && currHeight < heightDown && currHeight < heightLeft && currHeight < heightRight;
	}

	/**
	 * Gets the basin size.
	 *
	 * @param row        The row.
	 * @param column     The column.
	 * @param heightmaps The height maps.
	 * @param marked     The marked map. X if the height was added to basin. null if
	 *                   not yet added.
	 * @return The basin size.
	 */
	private static int getBasinSize(int row, int column, List<List<Integer>> heightmaps, String[][] marked) {
		int basinSize = 0;
		if (row >= 0 && row < heightmaps.size() && column >= 0 && column < heightmaps.get(0).size()) {
			if (heightmaps.get(row).get(column) < 9 && marked[row][column] == null) {
				basinSize++;
				marked[row][column] = "X";
				// check left
				basinSize += getBasinSize(row, column - 1, heightmaps, marked);
				// check right
				basinSize += getBasinSize(row, column + 1, heightmaps, marked);
				// check up
				basinSize += getBasinSize(row - 1, column, heightmaps, marked);
				// check down
				basinSize += getBasinSize(row + 1, column, heightmaps, marked);
			}
		}
		return basinSize;
	}
}
