package adventofcode.year_2021;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/8
 *
 * @author Amanda Truong
 */
public class Day8 {
	private static final String INPUT_FILE_PATH = "resources/2021/day8-input.txt";

	public static void answer() {
		System.out.println("\nDay 8 - ******** WIP ********");

		List<String> exampleLines = new ArrayList<>();
		exampleLines.add("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf");
		// exampleLines.add("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb
		// | fdgacbe cefdb cefbgd gcbe");
		// exampleLines.add("edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec
		// | fcgedb cgb dgebacf gc");
		// exampleLines.add("fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef
		// | cg cg fdcagb cbg");
		// exampleLines.add("fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega
		// | efabcd cedba gadfec cb");
		// exampleLines.add("aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga
		// | gecf egdcabf bgf bfgea");
		// exampleLines.add("fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf
		// | gebdcfa ecba ca fadegcb");
		// exampleLines.add("dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf
		// | cefg dcbef fcge gbcadfe");
		// exampleLines.add("bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd
		// | ed bcgafe cdgba cbgef");
		// exampleLines.add("egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg
		// | gbdfcae bgc cg cgb");
		// exampleLines.add("gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc
		// | fgae cfgab fg bagce");

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(inputLines);
		part2(exampleLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");
		int n = 0;
		for (String line : lines) {
			String[] delimiterSplit = line.split(" \\| ");
			// 4 output values
			String[] outputValues = delimiterSplit[1].trim().split(" ");
			n += Arrays.asList(outputValues).stream().filter(pattern -> isUniqueSegment(pattern)).count();
		}
		System.out.println("number of unique segments=" + n);
	}

	/**
	 * Whether or not the pattern is a unique segment.
	 *
	 * @param pattern The pattern.
	 * @return True if the pattern is a unique segment. False otherwise. The unique
	 *         segments are:
	 *         digit 1 = 2 segments, digit 4 = 4 segments, digit 7 = 3 segments,
	 *         digit 8 = 7 segments
	 */
	private static boolean isUniqueSegment(String pattern) {
		return pattern.length() == 2 || pattern.length() == 4 || pattern.length() == 3 || pattern.length() == 7;
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");

		int sum = 0;
		for (String line : lines) {
			String[] delimiterSplit = line.split(" \\| ");
			// unique signal patterns
			String[] digitMapping = mapDigits(delimiterSplit[0].trim());
			// 4 output values
			String[] outputValues = delimiterSplit[1].trim().split(" ");
			// String outputValue = Arrays.asList(outputValues).stream()
			// .map(pattern -> convertPatternToDigit(pattern, digitMapping))
			// .collect(Collectors.joining());
			// System.out.println(line + "=" + outputValue);
			// sum+=Integer.parseInt(outputValue);
		}
		// System.out.println("sum=" + sum);
	}

	/**
	 * Maps the unique signal patterns to its corresponding digits.
	 *
	 * @param uniqueSignalPatterns The String containing 10 unique signal patterns.
	 * @return The digit mapping. Each element represents the corresponding pattern
	 *         and the index representing
	 *         its digit.
	 */
	private static String[] mapDigits(String uniqueSignalPatterns) {
		String[] digitMapping = new String[10];
		String[] patterns = uniqueSignalPatterns.split(" ");
		// set digits with unique pattern lengths (1, 4, 7, 8)
		for (String pattern : patterns) {
			if (pattern.length() == 2) {
				digitMapping[1] = pattern;
			} else if (pattern.length() == 4) {
				digitMapping[4] = pattern;
			} else if (pattern.length() == 3) {
				digitMapping[7] = pattern;
			} else if (pattern.length() == 7) {
				digitMapping[8] = pattern;
			}
		}
		// TODO form configuration
		String topMiddle = null;
		String topLeft = null;
		String topRight = null;
		String middle = null;
		String bottomLeft = null;
		String bottomRight = null;
		String bottomMiddle = null;

		List<String> digitOneSegments = Arrays.asList(digitMapping[1].split(""));
		List<String> digitFourSegments = Arrays.asList(digitMapping[4].split(""));
		List<String> digitSevenSegments = Arrays.asList(digitMapping[7].split(""));
		List<String> digitEightSegments = Arrays.asList(digitMapping[8].split(""));

		// top middle
		topMiddle = digitSevenSegments.stream()
				.filter(segment -> !digitOneSegments.contains(segment))
				.collect(Collectors.toList()).get(0);
		System.out.println("topMiddle=" + topMiddle);

		// TODO top left
		digitFourSegments.stream().filter(segment -> !digitOneSegments.contains(segment)).collect(Collectors.toList());

		// TODO top right

		// TODO middle
		digitFourSegments.stream().filter(segment -> !digitOneSegments.contains(segment)).collect(Collectors.toList());

		// TODO bottom left
		List<String> bottomLeftSegments = digitEightSegments.stream()
				.filter(segment -> !digitFourSegments.contains(segment))
				.collect(Collectors.toList());
		bottomLeftSegments.remove(topMiddle);

		// TODO bottom right

		// TODO bottom middle
		List<String> bottomMiddleSegments = digitEightSegments.stream()
				.filter(segment -> !digitFourSegments.contains(segment))
				.collect(Collectors.toList());
		bottomMiddleSegments.remove(topMiddle);

		// TODO set remaining digits (0, 2, 3, 5, 6, 9)
		return digitMapping;
	}

	/**
	 * Converts the pattern to digit using the digit mapping.
	 *
	 * @param pattern      The pattern.
	 * @param digitMapping The digit mapping. Each element represents the
	 *                     corresponding pattern and the index
	 *                     representing its digit.
	 * @return The digit.
	 */
	private static String convertPatternToDigit(String pattern, String[] digitMapping) {
		String digit = null;
		List<String> patternSegments = Arrays.asList(pattern.split(""));
		Collections.sort(patternSegments);
		for (int i = 0; i < digitMapping.length; i++) {
			List<String> digitPatternSegments = Arrays.asList(digitMapping[2].split(""));
			Collections.sort(digitPatternSegments);
			if (patternSegments.equals(digitPatternSegments)) {
				digit = i + "";
				break;
			}
		}
		return digit;
	}
}
