package adventofcode.year_2021;

/**
 * https://adventofcode.com/2021
 *
 * @author Amanda Truong
 */
public class Year2021 {
	public static void answer() {
		System.out.println("\n**2021**");
		// Day1.answer();
		// Day2.answer();
		// Day3.answer();
		// Day4.answer();
		// Day5.answer();
		// Day6.answer();
		// Day7.answer();
		// Day8.answer(); //WIP
		// Day9.answer();
		// Day10.answer();
		// Day11.answer();
		Day12.answer();
		// Day13.answer();
		// Day14.answer();
		// Day15.answer();
		// Day16.answer();
		// Day17.answer();
	}
}
