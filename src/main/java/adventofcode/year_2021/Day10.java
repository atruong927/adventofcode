package adventofcode.year_2021;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2021/day/10
 *
 * @author Amanda Truong
 */
public class Day10 {
	private static final String INPUT_FILE_PATH = "resources/2021/day10-input.txt";

	public static void answer() {
		System.out.println("\nDay 10");

		List<String> exampleLines = new ArrayList<>();
		exampleLines.add("[({(<(())[]>[[{[]{<()<>>");
		exampleLines.add("[(()[<>])]({[<{<<[]>>(");
		exampleLines.add("{([(<{}[<>[]}>{[]{[(<()>");
		exampleLines.add("(((({<>}<{<{<>}{[]{[]{}");
		exampleLines.add("[[<[([]))<([[{}[[()]]]");
		exampleLines.add("[{[{({}]{}}([{[{{{}}([]");
		exampleLines.add("{<[[]]>}<{[{[{[]{()[[[]");
		exampleLines.add("[<(<(<(<{}))><([]([]()");
		exampleLines.add("<{([([[(<>()){}]>(<<{{");
		exampleLines.add("<{([{{}}[<[[[<>{}]]]>[]]");

		List<String> inputLines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(inputLines);
		part2(inputLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");

		// count corrupted lines (close with wrong character)
		int roundCount = 0;
		int squareCount = 0;
		int curlyCount = 0;
		int angleCount = 0;

		for (String line : lines) {
			String[] brackets = line.split("");
			LinkedList<String> bracketQueue = new LinkedList<>();

			for (String bracket : brackets) {
				// add open bracket to queue
				if (bracket.equals("(") || bracket.equals("[") || bracket.equals("{") || bracket.equals("<")) {
					bracketQueue.add(bracket);
				}
				// round close
				else if (bracket.equals(")")) {
					// remove valid close
					if (bracketQueue.peekLast().equals("(")) {
						bracketQueue.removeLast();
					}
					// first wrong close bracket of the line found
					else {
						roundCount++;
						break;
					}
				}
				// square close
				else if (bracket.equals("]")) {
					// remove valid close
					if (bracketQueue.peekLast().equals("[")) {
						bracketQueue.removeLast();
					}
					// first wrong close bracket of the line found
					else {
						squareCount++;
						break;
					}
				}
				// curly close
				else if (bracket.equals("}")) {
					// remove valid close
					if (bracketQueue.peekLast().equals("{")) {
						bracketQueue.removeLast();
					}
					// first wrong close bracket of the line found
					else {
						curlyCount++;
						break;
					}
				}
				// angle close
				else if (bracket.equals(">")) {
					// remove valid close
					if (bracketQueue.peekLast().equals("<")) {
						bracketQueue.removeLast();
					}
					// first wrong close bracket of the line found
					else {
						angleCount++;
						break;
					}
				}
			}
		}

		// calculate points
		int roundTotal = roundCount * 3;
		int squareTotal = squareCount * 57;
		int curlyTotal = curlyCount * 1197;
		int angleTotal = angleCount * 25137;
		int points = roundTotal + squareTotal + curlyTotal + angleTotal;
		System.out.println("points=" + points);
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");

		List<Long> scoreList = new ArrayList<>();

		for (String line : lines) {
			String[] brackets = line.split("");
			LinkedList<String> bracketQueue = new LinkedList<>();

			boolean isCorrupt = false;

			for (String bracket : brackets) {
				// add open bracket to queue
				if (bracket.equals("(") || bracket.equals("[") || bracket.equals("{") || bracket.equals("<")) {
					bracketQueue.add(bracket);
				}
				// round close
				else if (bracket.equals(")")) {
					// remove valid close
					if (bracketQueue.peekLast().equals("(")) {
						bracketQueue.removeLast();
					}
					// first wrong close bracket of the line found
					else {
						isCorrupt = true;
						break;
					}
				}
				// square close
				else if (bracket.equals("]")) {
					// remove valid close
					if (bracketQueue.peekLast().equals("[")) {
						bracketQueue.removeLast();
					}
					// first wrong close bracket of the line found
					else {
						isCorrupt = true;
						break;
					}
				}
				// curly close
				else if (bracket.equals("}")) {
					// remove valid close
					if (bracketQueue.peekLast().equals("{")) {
						bracketQueue.removeLast();
					}
					// first wrong close bracket of the line found
					else {
						isCorrupt = true;
						break;
					}
				}
				// angle close
				else if (bracket.equals(">")) {
					// remove valid close
					if (bracketQueue.peekLast().equals("<")) {
						bracketQueue.removeLast();
					}
					// first wrong close bracket of the line found
					else {
						isCorrupt = true;
						break;
					}
				}
			}

			// count incomplete line's brackets and calculate score
			if (!isCorrupt) {
				long score = 0;

				while (!bracketQueue.isEmpty()) {
					String incompleteBracket = bracketQueue.removeLast();
					switch (incompleteBracket) {
						case "(":
							// 1 point
							score = score * 5l + 1;
							break;
						case "[":
							// 2 points
							score = score * 5l + 2;
							break;
						case "{":
							// 3 points
							score = score * 5l + 3;
							break;
						case "<":
							// 4 points
							score = score * 5l + 4;
							break;
					}
				}

				scoreList.add(score);
			}
		}

		// find middle score
		Collections.sort(scoreList);
		Long middle = scoreList.get(scoreList.size() / 2);
		System.out.println("middle score=" + middle);
	}
}
