package adventofcode.year_2022;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2022/day/5
 *
 * @author Amanda Truong
 */
public class Day5 {
	private static final String INPUT_FILE_PATH = "resources/2022/day5-input.txt";

	public static void answer() {
		System.out.println("\nDay 5");

		// Load input file
		List<String> exampleLines = new ArrayList<>();
		exampleLines.add("    [D]");
		exampleLines.add("[N] [C]");
		exampleLines.add("[Z] [M] [P]");
		exampleLines.add(" 1   2   3");
		exampleLines.add("");
		exampleLines.add("move 1 from 2 to 1");
		exampleLines.add("move 3 from 1 to 3");
		exampleLines.add("move 2 from 2 to 1");
		exampleLines.add("move 1 from 1 to 2");

		List<String> lines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(lines);
		part2(lines);
	}

	private static int findStackNumIndex(List<String> lines) {
		// Find index contaiing number of stacks
		int stackNumIndex = -1;
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			if(line.isEmpty()) {
				stackNumIndex = i - 1;
				break;
			}
		}
		return stackNumIndex;
	}

	private static Map<Integer, LinkedList<String>> parseStacks(int stackNumIndex, List<String> lines) {
		// Find number of stacks

		// Fill stacks
		Map<Integer, LinkedList<String>> stacks = new HashMap<>();
		for (int i = 0; i < stackNumIndex; i++) {
			String line = lines.get(i);
			// letter on every 4 spaces
			int stackNum = 1;
			for (int j = 1; j < line.length(); j+=4) {
				String crate = line.substring(j, j+1);
				if(!crate.trim().isEmpty()) {
					stacks.putIfAbsent(stackNum, new LinkedList<>());
					stacks.get(stackNum).push(crate);
				}
				stackNum++;
			}
		}

		return stacks;
	}

	private static List<int[]> parseSteps(int stackNumIndex, List<String> lines) {
		// Format steps to int array [num crates to move, from, to]
		List<String> steps = lines.subList(stackNumIndex + 2, lines.size());
		List<int[]> stepsFormatted = steps.stream().map(step -> {

			// Remove "move" from step
			step = step.substring(5);

			// Split out numbers
			String[] stepSplit1 = step.split("from");
			String numCratesToMove = stepSplit1[0].trim();

			String[] stepSplit2 = stepSplit1[1].trim().split("to");
			String from = stepSplit2[0].trim();
			String to = stepSplit2[1].trim();

			return new int[]{Integer.parseInt(numCratesToMove), Integer.parseInt(from), Integer.parseInt(to)};

		}).collect(Collectors.toList());
		return stepsFormatted;
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");

		// Find index containing number of stacks
		int stackNumIndex = findStackNumIndex(lines);

		// Fill stacks with crates
		Map<Integer, LinkedList<String>> stacks = parseStacks(stackNumIndex, lines);

		// Get formatted steps
		List<int[]> steps = parseSteps(stackNumIndex, lines);

		// Move crates around stacks, based on steps; 2 past stack num index to include empty line
		for (int[] step : steps) {
			int numCratesToMove = step[0];
			int from = step[1];
			int to = step[2];

			// Moving 1 crate at a time
			for (int i = 1; i <= numCratesToMove; i++) {
				stacks.get(to).add(stacks.get(from).removeLast());
			}

		}

		// Find the crates that are on the top of each stack
		String topCrates = "";
		for (int i = 1; i <= stacks.size(); i++) {
			topCrates = topCrates + stacks.get(i).getLast();
		}
		System.out.println("Top Crates = " + topCrates);
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");

		// Find index containing number of stacks
		int stackNumIndex = findStackNumIndex(lines);

		// Fill stacks with crates
		Map<Integer, LinkedList<String>> stacks = parseStacks(stackNumIndex, lines);

		// Get formatted steps
		List<int[]> steps = parseSteps(stackNumIndex, lines);

		// Move crates around stacks, based on steps; 2 past stack num index to include empty line
		for (int[] step : steps) {
			int numCratesToMove = step[0];
			int from = step[1];
			int to = step[2];

			// Moving a whole group of crates at once
			List<String> removedStack = new LinkedList<>();
			for (int i = 1; i <= numCratesToMove; i++) {
				removedStack.add(0, stacks.get(from).removeLast());
			}

			stacks.get(to).addAll(removedStack);

		}

		// Find the crates that are on the top of each stack
		String topCrates = "";
		for (int i = 1; i <= stacks.size(); i++) {
			topCrates = topCrates + stacks.get(i).getLast();
		}
		System.out.println("Top Crates = " + topCrates);
	}
}
