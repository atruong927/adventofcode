package adventofcode.year_2022;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2022/day/2
 *
 * @author Amanda Truong
 */
public class Day2 {
	private static final String INPUT_FILE_PATH = "resources/2022/day2-input.txt";

	public static void answer() {
		System.out.println("\nDay 2");

		// Load input file
		List<String> exampleLines = Arrays.asList("A Y", "B X", "C Z");

		List<String> lines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(lines);
		part2(lines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");

		// First Column = Opponent
		// Second Column = Response

		// Parse RPS Rounds
		List<String[]> rounds = lines.stream()
			.map(line -> {
				String[] round = line.split(" ");

				// A = X = Rock; B = Y = Paper; C = Z = Scissor
				for (int i = 0; i < round.length; i++) {
					if(round[i].equals("A") || round[i].equals("X")) {
						round[i] = "Rock";
					}
					else if(round[i].equals("B") || round[i].equals("Y")) {
						round[i] = "Paper";
					}
					else if(round[i].equals("C") || round[i].equals("Z")) {
						round[i] = "Scissor";
					}
				}
				return round;
			})
			.collect(Collectors.toList());
		// System.out.println("Opponent - Response");
		// for (String[] round : rounds) {
		// 	System.out.println(round[0] + " - " + round[1]);
		// }

		// Scoring
		List<Integer> scores = rounds.stream()
			.map(round -> {
				int score = 0;
				// Add points for response
				// Rock = 1 Point; Paper = 2 Points; Scissor = 3 Points
				if(round[1].equals("Rock")) {
					score += 1;
				}
				else if(round[1].equals("Paper")) {
					score += 2;
				}
				else if(round[1].equals("Scissor")) {
					score += 3;
				}

				// Add points for outcome
				// Lose = 0 Points; Draw = 3 Points; Win = 6 Points
				// Draw
				if(round[0].equals(round[1])) {
					score += 3;
				}
				// Wins
				else if(round[0].equals("Rock") && round[1].equals("Paper")) {
					score += 6;
				}
				else if(round[0].equals("Paper") && round[1].equals("Scissor")) {
					score += 6;
				}
				else if(round[0].equals("Scissor") && round[1].equals("Rock")) {
					score += 6;
				}
				return score;
			})
			.collect(Collectors.toList());
		// System.out.println("scores=" + scores);

		// Find total score after all rounds
		int totalScore = 0;
		for (int score : scores) {
			totalScore += score;
		}
		System.out.println("Total Score = " + totalScore);
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");

		// First Column = Opponent
		// Second Column = Outcome

		// Parse RPS Rounds
		List<String[]> rounds = lines.stream()
			.map(line -> {
				String[] round = line.split(" ");

				// A = Rock; B = Paper; C = Scissor
				// X = Lose; Y = Draw; Z = Win
				for (int i = 0; i < round.length; i++) {
					if(round[i].equals("A")) {
						round[i] = "Rock";
					}
					else if(round[i].equals("B")) {
						round[i] = "Paper";
					}
					else if(round[i].equals("C")) {
						round[i] = "Scissor";
					}
					else if(round[i].equals("X")) {
						round[i] = "Lose";
					}
					else if(round[i].equals("Y")) {
						round[i] = "Draw";
					}
					else if(round[i].equals("Z")) {
						round[i] = "Win";
					}
				}
				return round;
			})
			.collect(Collectors.toList());

		// System.out.println("Opponent - Outcome");
		// for (String[] round : rounds) {
		// 	System.out.println(round[0] + " - " + round[1]);
		// }

		// Scoring
		List<Integer> scores = rounds.stream()
			.map(round -> {
				String opponent = round[0];
				String outcome = round[1];

				// Find response, based on outcome
				String response = null;
				if(outcome.equals("Lose")) {
					if(opponent.equals("Rock")) {
						response = "Scissor";
					}
					else if(opponent.equals("Paper")) {
						response = "Rock";
					}
					else if(opponent.equals("Scissor")) {
						response = "Paper";
					}
				}
				else if(outcome.equals("Draw")) {
					response = opponent;
				}
				else if(outcome.equals("Win")) {
					if(opponent.equals("Rock")) {
						response = "Paper";
					}
					else if(opponent.equals("Paper")) {
						response = "Scissor";
					}
					else if(opponent.equals("Scissor")) {
						response = "Rock";
					}
				}

				// System.out.println("Opponent - Response - Outcome");
				// System.out.println(opponent + " - " + response + " - " + outcome);

				int score = 0;
				// Add points for response
				// Rock = 1 Point; Paper = 2 Points; Scissor = 3 Points
				if(response.equals("Rock")) {
					score += 1;
				}
				else if(response.equals("Paper")) {
					score += 2;
				}
				else if(response.equals("Scissor")) {
					score += 3;
				}

				// Add points for outcome
				// Lose = 0 Points; Draw = 3 Points; Win = 6 Points
				if(outcome.equals("Lose")) {
					score += 0;
				}
				else if(outcome.equals("Draw")) {
					score += 3;
				}
				else if(outcome.equals("Win")) {
					score += 6;
				}
				return score;
			})
			.collect(Collectors.toList());
		// System.out.println("scores=" + scores);

		// Find total score after all rounds
		int totalScore = 0;
		for (int score : scores) {
			totalScore += score;
		}
		System.out.println("Total Score = " + totalScore);
	}
}
