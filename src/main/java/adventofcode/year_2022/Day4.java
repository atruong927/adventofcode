package adventofcode.year_2022;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2022/day/4
 *
 * @author Amanda Truong
 */
public class Day4 {
	private static final String INPUT_FILE_PATH = "resources/2022/day4-input.txt";

	public static void answer() {
		System.out.println("\nDay 4");

		// Load input file
		List<String> exampleLines = new ArrayList<>();
		exampleLines.add("2-4,6-8");
		exampleLines.add("2-3,4-5");
		exampleLines.add("5-7,7-9");
		exampleLines.add("2-8,3-7");
		exampleLines.add("6-6,4-6");
		exampleLines.add("2-6,4-8");

		List<String> lines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(lines);
		part2(lines);
	}

	private static List<List<List<Integer>>> getPairs(List<String> lines) {
		return lines.stream()
			.map(line -> line.split(","))
			.map(pair -> {
				List<Integer> elf1 = Arrays.asList(pair[0].split("-")).stream()
					.map(section -> Integer.parseInt(section))
					.collect(Collectors.toList());
				List<Integer> elf2 = Arrays.asList(pair[1].split("-")).stream()
					.map(section -> Integer.parseInt(section))
					.collect(Collectors.toList());
				return Arrays.asList(elf1, elf2);
			})
			.collect(Collectors.toList());
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");

		// Parse line to elf pairs
		List<List<List<Integer>>> pairs = getPairs(lines);

		// Count number of pairs that fully contains the other
		int counter = 0;
		for (List<List<Integer>> pair : pairs) {
			List<Integer> elf1 = pair.get(0);
			List<Integer> elf2 = pair.get(1);
			int x1 = elf1.get(0);
			int y1 = elf1.get(1);
			int x2 = elf2.get(0);
			int y2 = elf2.get(1);

			// pair is in another pair (x1 y1, x2 y2)
			// x1 is between x2 and y2 and y1 is between x2 and y2
			// x2 is between x1 and y1 and y2 is between x1 and y1
			if(x1 >= x2 && x1 <= y2 && y1 >= x2 && y1 <= y2) {
				counter++;
			}
			else if(x2 >= x1 && x2 <= y1 && y2 >= x1 && y2 <= y1) {
				counter++;
			}
		}

		System.out.println("counter=" + counter);
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");

		// Parse line to elf pairs
		List<List<List<Integer>>> pairs = getPairs(lines);

		// TODO Count number of pairs that overlap the other
		int counter = 0;
		for (List<List<Integer>> pair : pairs) {
			List<Integer> elf1 = pair.get(0);
			List<Integer> elf2 = pair.get(1);
			int x1 = elf1.get(0);
			int y1 = elf1.get(1);
			int x2 = elf2.get(0);
			int y2 = elf2.get(1);

			// pair is overlapping another pair (x1 y1, x2 y2)
			// x1 is between x2 and y2 or y1 is between x2 and y2
			// x2 is between x1 and y1 or y2 is between x1 and y1

			if(x1 >= x2 && x1 <= y2 || y1 >= x2 && y1 <= y2) {
				counter++;
			}
			else if(x2 >= x1 && x2 <= y1 || y2 >= x1 && y2 <= y1) {
				counter++;
			}
		}

		System.out.println("counter=" + counter);
	}
}
