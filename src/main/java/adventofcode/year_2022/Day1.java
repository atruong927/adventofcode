package adventofcode.year_2022;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2022/day/1
 *
 * @author Amanda Truong
 */
public class Day1 {
	private static final String INPUT_FILE_PATH = "resources/2022/day1-input.txt";

	public static void answer() {
		System.out.println("\nDay 1");

		// Load input file
		List<String> exampleLines = Arrays.asList("1000", "2000", "3000", "", "4000", "", "5000", "6000", "",
			"7000", "8000", "9000", "", "10000");

		List<String> lines = DataUtil.readFileLines(INPUT_FILE_PATH);
		List<Integer> elfs = getElfs(lines);

		part1(elfs);
		part2(elfs);
	}

	public static List<Integer> getElfs(List<String> lines) {
		// Each Elf's food calories separated by blank line
		List<Integer> elfs = new ArrayList<>();
		int currElf = 0;
		for (String line : lines) {
			if(!line.isEmpty()) {
				currElf += Integer.parseInt(line);
			}
			else {
				elfs.add(currElf);
				currElf = 0;
			}
		}
		elfs.add(currElf);
		return elfs;
	}

	private static void part1(List<Integer> elfs) {
		System.out.println("Part 1");

		// Find the elf with the most calories
		Collections.sort(elfs);
		int highestCalories = elfs.get(elfs.size()-1);

		System.out.println("Highest Calories = " + highestCalories);
	}

	private static void part2(List<Integer> elfs) {
		System.out.println("Part 2");

		// Find top 3 elves and get the sum of their calories
		Collections.sort(elfs);
		int highestCalories = elfs.get(elfs.size()-1) + elfs.get(elfs.size()-2) + elfs.get(elfs.size()-3);

		System.out.println("Highest Calories = " + highestCalories);
	}
}
