package adventofcode.year_2022;

import java.util.Arrays;
import java.util.List;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2022/day/7
 *
 * @author Amanda Truong
 */
public class Day7 {
	private static final String INPUT_FILE_PATH = "resources/2022/day7-input.txt";

	public static void answer() {
		System.out.println("\nDay 7");

		// Load input file
		List<String> exampleLines = Arrays.asList();

		List<String> lines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(exampleLines);
		part2(exampleLines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");
	}
}
