package adventofcode.year_2022;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2022/day/3
 *
 * @author Amanda Truong
 */
public class Day3 {
	private static final String INPUT_FILE_PATH = "resources/2022/day3-input.txt";

	public static void answer() {
		System.out.println("\nDay 3");

		// Load input file
		List<String> exampleLines = Arrays.asList("vJrwpWtwJgWrhcsFMMfFFhFp", "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
			"PmmdzqPrVvPwwTWBwg", "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn", "ttgJtRGJQctTZtZT", "CrZsJsPPZsGzwwsLwLmpwMDw");

		List<String> lines = DataUtil.readFileLines(INPUT_FILE_PATH);

		part1(lines);
		part2(lines);
	}

	private static void part1(List<String> lines) {
		System.out.println("Part 1");

		// Parse each rucksack and its compartments
		List<String[][]> rucksacks = lines.stream()
			.map(line -> {
				// Rucksack split into 2 compartments: 1st half = 1st Compartment, 2nd half = 2nd Compartment
				// String[] compartments = new String[2];
				String[] compartment1 = line.substring(0, line.length() / 2).split("");
				String[] compartment2 = line.substring(line.length() / 2).split("");
				String[][] rucksack = {compartment1, compartment2};
				return rucksack;
			})
			.collect(Collectors.toList());

		// Each letter (lower or upper case) represents a different item
		// Find items that appear in both compartments for each rucksack
		List<String> matchingItems = new ArrayList<>();
		for (String[][] rucksack : rucksacks) {
			List<String> compartment1 = Arrays.asList(rucksack[0]);
			List<String> compartment2 = Arrays.asList(rucksack[1]);

			for (String item : compartment1) {
				if(compartment2.contains(item)) {
					matchingItems.add(item);
					break;
				}
			}
		}

		// Find the sum of all matching items' priorities
		int sum = getSum(matchingItems);
		System.out.println("Sum = " + sum);
	}

	private static void part2(List<String> lines) {
		System.out.println("Part 2");

		// Parse each group of 3 (lines)
		List<String[][]> groups = new ArrayList<>();
		for (int i = 0; i < lines.size(); i+=3) {
			String[][] group = {
				lines.get(i).split(""),
				lines.get(i+1).split(""),
				lines.get(i+2).split("")
			};
			groups.add(group);
		}

		// Each letter (lower or upper case) represents a different item
		// Find items that appear in both compartments for each rucksack
		List<String> matchingItems = new ArrayList<>();
		for (String[][] group : groups) {
			List<String> elf1 = Arrays.asList(group[0]);
			List<String> elf2 = Arrays.asList(group[1]);
			List<String> elf3 = Arrays.asList(group[2]);

			for (String item : elf1) {
				if(elf2.contains(item) && elf3.contains(item)) {
					matchingItems.add(item);
					break;
				}
			}
		}

		// Find the sum of all matching items' priorities
		int sum = getSum(matchingItems);
		System.out.println("Sum = " + sum);
	}

	private static int getSum(List<String> matchingItems) {
		// Item Priorities: a - z = 1 - 26; A - Z = 27 - 52
		String alphabetString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		List<Integer> matchingItemPriorities = matchingItems.stream()
			.map(item -> {
				int priority = alphabetString.indexOf(item) + 1;

				return priority;
			})
			.collect(Collectors.toList());

		// Find the sum of all item priorities
		int sum = 0;
		for (Integer priority : matchingItemPriorities) {
			sum += priority;
		}
		return sum;
	}
}
