package adventofcode.year_2022;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import adventofcode.util.DataUtil;

/**
 * https://adventofcode.com/2022/day/6
 *
 * @author Amanda Truong
 */
public class Day6 {
	private static final String INPUT_FILE_PATH = "resources/2022/day6-input.txt";

	public static void answer() {
		System.out.println("\nDay 6");

		// Load input file
		String line = DataUtil.readFileLines(INPUT_FILE_PATH).get(0);

		// Examples
		// part1("bvwbjplbgvbhsrlpgdmjqwftvncz");
		// part1("nppdvjthqldpwncqszvftbrmjlhg");
		// part1("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg");
		// part1("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw");
		// File input
		part1(line);

		// Examples
		// part2("mjqjpqmgbljsphdztnvjfqwrcgsmlb");
		// part2("bvwbjplbgvbhsrlpgdmjqwftvncz");
		// part2("nppdvjthqldpwncqszvftbrmjlhg");
		// part2("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg");
		// part2("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw");
		// File input
		part2(line);
	}

	private static void part1(String signal) {
		System.out.println("Part 1");

		// Find start-of-packet marker (4 characters all diff)
		int numChars = 4;
		String[] characters = signal.split("");

		int firstMarker = 0;
		for (int i = 0; i < characters.length; i++) {
			Set<String> charSet = new HashSet<>();
			for (int j = 0; j < numChars; j++) {
				charSet.add(characters[i + j]);
			}

			// No duplicates would exist in a set if all characters were different
			if(charSet.size() == numChars) {
				firstMarker = i + numChars;
				break;
			}
		}

		// Find character number of first marker
		System.out.println("First Marker = " + firstMarker);
	}

	private static void part2(String signal) {
		System.out.println("Part 2");

		// Find start-of-packet marker (4 characters all diff)
		int numChars = 14;
		String[] characters = signal.split("");

		int firstMarker = 0;
		for (int i = 0; i < characters.length; i++) {
			Set<String> charSet = new HashSet<>();
			for (int j = 0; j < numChars; j++) {
				charSet.add(characters[i + j]);
			}

			// No duplicates would exist in a set if all characters were different
			if(charSet.size() == numChars) {
				firstMarker = i + numChars;
				break;
			}
		}

		// Find character number of first marker
		System.out.println("First Marker = " + firstMarker);
	}
}
